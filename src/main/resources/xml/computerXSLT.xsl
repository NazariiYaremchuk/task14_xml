<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body>
                <h2>
                    View of devices
                </h2>
                <table border="1">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Origin</th>
                        <th>Price</th>
                        <th>IsPerifery</th>
                        <th>Energy Consuption</th>
                        <th>Is Cooler</th>
                        <th>Device Group</th>
                        <th>Ports</th>
                        <th>Is Critical</th>
                    </tr>
                    <xsl:for-each select="devices/device">
                        <xsl:sort select="price"/>
                        <tr>

                            <td>
                                <xsl:value-of select="attribute::id"/>
                            </td>
                            <td>
                                <xsl:value-of select="name"/>
                            </td>
                            <td>
                                <xsl:value-of select="origin"/>
                            </td>
                            <td>
                                <xsl:value-of select="price"/>
                            </td>
                            <td>
                                <xsl:value-of select="type/isPerifery"/>
                            </td>
                            <td>
                                <xsl:value-of select="type/energyConsumption"/>
                            </td>
                            <td>
                                <xsl:value-of select="type/isCooler"/>
                            </td>
                            <td>
                                <xsl:value-of select="type/complectingGroup/deviceGroup"/>
                            </td>
                            <td>
                                <xsl:value-of select="type/complectingGroup/ports"/>
                            </td>
                            <td>
                                <xsl:value-of select="isCritical"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
