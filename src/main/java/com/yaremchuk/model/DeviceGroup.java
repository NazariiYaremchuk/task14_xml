package com.yaremchuk.model;

public enum DeviceGroup {
    INPUT, OUTPUT, MULTIMEDIA
}
