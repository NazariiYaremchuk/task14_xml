package com.yaremchuk.parser;

import com.yaremchuk.model.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DOMParser {
    private static List<Device> devices = new ArrayList<>();

    public void init() throws IOException, SAXException, ParserConfigurationException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();

        Document document = builder.parse(new File("src/main/resources/xml/computerXML.xml"));

        Schema schema = null;
        try {
            String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
            SchemaFactory schemaFactory = SchemaFactory.newInstance(language);
            schema = schemaFactory.newSchema(new File("src/main/resources/xml/computerXSD.xsd"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Validator validator = schema.newValidator();
        validator.validate(new DOMSource(document));

        //Normalize the XML Structure; It's just too important !!
        document.getDocumentElement().normalize();

        Element root = document.getDocumentElement();
        System.out.println(root.getNodeName());

        //Get all employees
        NodeList nList = document.getElementsByTagName("device");
        System.out.println("============================");
        visitChildNodes(nList);

        System.out.println(devices);
    }

    private static void visitChildNodes(NodeList nList) {
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node node = nList.item(temp);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                processNode(node);
            }
        }

    }
    public static void processNode(Node node) {
        Element eElement = (Element) node;
        ComplectingGroup complectingGroup = new ComplectingGroup(
                DeviceGroup.valueOf(eElement.getElementsByTagName("deviceGroup").item(0).getTextContent()),
                Ports.valueOf(eElement.getElementsByTagName("ports").item(0).getTextContent())

        );

        Type type = new Type(
                Boolean.getBoolean(eElement.getAttribute("isPerifery")),
                Double.parseDouble(eElement.getElementsByTagName("energyConsumption").item(0).getTextContent()),
                Boolean.getBoolean(eElement.getElementsByTagName("isCooler").item(0).getTextContent()),
                complectingGroup
        );

        Device device = new Device(
                Integer.parseInt(eElement.getAttribute("id")),
                eElement.getElementsByTagName("name").item(0).getTextContent(),
                eElement.getElementsByTagName("origin").item(0).getTextContent(),
                Integer.parseInt(eElement.getElementsByTagName("price").item(0).getTextContent()),
                type,
                Boolean.getBoolean(eElement.getElementsByTagName("isCritical").item(0).getTextContent())
        );

        devices.add(device);
    }

    public static void main(String[] args) {
        DOMParser parser = new DOMParser();
        try {
            parser.init();
        } catch (IOException | ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }
    }
}