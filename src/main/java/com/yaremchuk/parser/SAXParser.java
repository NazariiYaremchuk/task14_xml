package com.yaremchuk.parser;

import com.yaremchuk.model.Device;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;


import java.io.*;
import java.util.ArrayList;

public class SAXParser {
    public ArrayList parseXml(InputStream in) throws SAXException {
        //Create a empty link of users initially
        ArrayList<Device> users = new ArrayList();
        try
        {
            //Create default handler instance
            SAXHandler handler = new SAXHandler();

            //Create parser from factory
            XMLReader parser = XMLReaderFactory.createXMLReader();

            //Register handler with parser
            parser.setContentHandler(handler);

            //Create an input source from the XML input stream
            InputSource source = new InputSource(in);

            //parse the document
            parser.parse(source);

            //populate the parsed users list in above created empty list; You can return from here also.
            users = handler.getUsers();

        } catch (SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {

        }
        return users;
    }

    public static void main(String[] args) throws FileNotFoundException, SAXException {
        //Locate the file
        File xmlFile = new File("src/main/resources/xml/computerXML.xml");

        //Create the parser instance
        SAXParser parser = new SAXParser();

        //Parse the file
        ArrayList devices = parser.parseXml(new FileInputStream(xmlFile));

        //Verify the result
        System.out.println(devices);
    }
}
